﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public struct Puzzle
{
	public Puzzle(string _code, int min_number_of_moves)
	{
		this.code = _code;
		this.min_moves = min_number_of_moves;
	}

	public string code { get; private set; }
	public int min_moves { get; private set; }
}

public class MainControl : MonoBehaviour 
{
	public static MainControl mainControl;
	public static int current_level;
	public static bool sound_on;
	public static List<Puzzle> puzzles = new List<Puzzle>();

	void Start () 
	{
		puzzles = new List<Puzzle>();
		puzzles.Add (new Puzzle ("t0N56L0N45", 0)); // puzzle 0 is never used for anything.

		puzzles.Add (new Puzzle ("t0N23B0N63B0N56", 2)); // 1st level: introduce drag mechanic.
		puzzles.Add (new Puzzle ("t0N26B0N56B0N44B0N85B0N73", 6)); // 2nd level: demonstrate that 'passing over' the target doesn't win.
		puzzles.Add (new Puzzle ("t0N56B0N33O0N83",2)); // 3rd level: demonstrate that tetrominos can be dragged too. 
		puzzles.Add (new Puzzle ("t0N37L0N34B0N77B0N65", 6)); // L-only.
		puzzles.Add (new Puzzle ("t0N28O0N57B0N35B0N74B0N86",9)); // O-only level.

		puzzles.Add (new Puzzle ("t0N66S1N63O0N46B0N23", 5)); // 
		puzzles.Add (new Puzzle ("t0N48S0F45T1N87B0N34", 6)); // 
		puzzles.Add (new Puzzle ("t0N64L2N85S0N52T3N45", 0)); //       very easy puzzle. good for physical game?
		puzzles.Add (new Puzzle ("t0N23L3N37S0N63T1N66", 0)); //       easy. good for physical game?
		puzzles.Add (new Puzzle ("t0N57L0N24O0N83S0N42", 0)); //       good for physical game?

		current_level = 1;
		sound_on = true;
	}

}
