﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockController : MonoBehaviour {

	[SerializeField] private GameObject eyes_go;
	[SerializeField] Sprite eyes_center;
	[SerializeField] Sprite eyes_up;
	[SerializeField] Sprite eyes_right;
	[SerializeField] Sprite eyes_down;
	[SerializeField] Sprite eyes_left;
	[SerializeField] Sprite eyes_closed;
	private SpriteRenderer eyes_sr;
	private Rigidbody2D rb;
	private Transform image_transform;
	private PuzzleSceneControl psc;

	private string type;
	private bool is_being_dragged;
	private bool xy_swap;

	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		rb.mass = 100.0f;
		image_transform = this.gameObject.transform.GetChild (0);
		is_being_dragged = false;
		eyes_sr = eyes_go.GetComponent<SpriteRenderer> ();
	}

	public void setPSC(PuzzleSceneControl psControl, string piece_type, int rota, bool flipped) 
	{
		this.psc = psControl;
		this.type = piece_type;

		if (rota == 1) {
			eyes_go.transform.Rotate (Vector3.forward * 90);
			if (flipped) {
				eyes_go.transform.localScale = new Vector3(0.7f,-0.6f,0.8f);
			} else {
				eyes_go.transform.localScale = new Vector3(0.7f,0.6f,0.8f);
			}
			xy_swap = true;
		} else if (rota == 2) {
			eyes_go.transform.Rotate (Vector3.forward * 180);
			if (flipped) {
				eyes_go.transform.localScale = new Vector3(-0.7f,0.6f,0.8f);
			} else {
				eyes_go.transform.localScale = new Vector3(0.7f,0.6f,0.8f);
			}
			xy_swap = false;
		} else if (rota == 3) {
			eyes_go.transform.Rotate (Vector3.forward * 270);
			if (flipped) {
				eyes_go.transform.localScale = new Vector3(0.7f,-0.6f,0.8f);
			} else {
				eyes_go.transform.localScale = new Vector3(0.7f,0.6f,0.8f);
			}
			xy_swap = true;
		} else {
			if (flipped) {
				eyes_go.transform.localScale = new Vector3(-0.7f,0.6f,0.8f);
			} else {
				eyes_go.transform.localScale = new Vector3(0.7f,0.6f,0.8f);
			}
			xy_swap = false;
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		eyes_sr.sprite = eyes_center;
		image_transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		if (coll.gameObject.tag == "Wall") {
			psc.failPuzzle();
			halt ();
		} else {
			GetComponent<blinking_script> ().can_blink = true;
			halt ();
			if (type == "t" && rb.position.x == 0.0f && rb.position.y == 0.0f) {
				psc.something_bumped (true);
				psc.solvePuzzle ();
			} else {
				psc.something_bumped (false);
			}
		}
	}

	private Vector3 startDragPoint, endDragPoint;
	void OnMouseDown()
	{
		if (psc.can_make_a_move) {
			is_being_dragged = true;
			startDragPoint = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 10.0f));
			GetComponent<blinking_script> ().can_blink = false;
		} else {
			is_being_dragged = false;
		}
	}
	void OnMouseDrag()
	{
		if (is_being_dragged) {
			endDragPoint = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 10.0f));
			Vector2 delta = getMomentum (endDragPoint - startDragPoint);
			if (delta.x > 0.1f && delta.y == 0.0f) {
				eyes_sr.sprite = eyes_right;
			} else if (delta.x == 0.0f && delta.y > 0.1f) {
				eyes_sr.sprite = eyes_up;
			} else if (delta.x < -0.1f && delta.y == 0.0f) {
				eyes_sr.sprite = eyes_left;
			} else if (delta.x == 0.0f && delta.y < -0.1f) {
				eyes_sr.sprite = eyes_down;
			} else {
				eyes_sr.sprite = eyes_center;
			}
		}
	}
	void OnMouseUp()
	{
		GetComponent<blinking_script> ().can_blink = true;
		if (is_being_dragged) {
			is_being_dragged = false;
			if (psc.can_make_a_move) {
				endDragPoint = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 10.0f));

				rb.velocity = getMomentum (endDragPoint - startDragPoint);
				if (rb.velocity.x != 0.0f) {
					if (xy_swap) {
						image_transform.localScale = new Vector3 (0.95f, 1.05f, 1.0f);
					} else {
						image_transform.localScale = new Vector3 (1.05f, 0.95f, 1.0f);
					}
					GetComponent<blinking_script> ().can_blink = false;
					psc.can_make_a_move = false;
					rb.mass = 1.0f;
				} else if (rb.velocity.y != 0.0f) {
					if (xy_swap) {
						image_transform.localScale = new Vector3 (1.05f, 0.95f, 1.0f);
					} else {
						image_transform.localScale = new Vector3 (0.95f, 1.05f, 1.0f);
					}
					GetComponent<blinking_script> ().can_blink = false;
					psc.can_make_a_move = false;
					rb.mass = 1.0f;
				}
			}
		}
	}

	Vector2 getMomentum(Vector3 delta)
	{ 
		if (Mathf.Abs (delta.y) > 0.1f + Mathf.Abs (delta.x)) {
			if (delta.y < -0.5f)
				return new Vector2 (0.0f, -8.0f);
			else if (delta.y > 0.5f)
				return new Vector2 (0.0f, 8.0f);
			else
				return new Vector2 (0.0f, 0.0f);
		} else if (Mathf.Abs (delta.x) > 0.1f + Mathf.Abs (delta.y)) {
			if (delta.x < -0.5f)
				return new Vector2 (-8.0f, 0.0f);
			else if (delta.x > 0.5f)
				return new Vector2 (8.0f, 0.0f);
			else
				return new Vector2 (0.0f, 0.0f);
		} else {
			return new Vector2 (0.0f, 0.0f);
		}
	}

	public void halt () 
	{
		rb.velocity = new Vector2 (0.0f, 0.0f);
		double x = Math.Round ( rb.position.x );
		double y = Math.Round ( rb.position.y );
		rb.position = new Vector2 ((float)x,(float)y);
		rb.mass = 100.0f;
		eyes_sr.sprite = eyes_center;
	}
}
