R = 20 # radius
S = 10 # spacing


L_str = '<svg version="1.1" baseProfile="full" width="360" height="360" xmlns="http://www.w3.org/'+str(S+R)+'00/svg">\n'
L_str += '<path d="'
L_str += 'M '+str(240-S-R)+' '+str(S)+' '
L_str += 'A '+str(R)+' '+str(R)+' 0 0 1 '+str(240-S)+' '+str(S+R)+' '
L_str += 'L '+str(240-S)+' '+str(240+S-R)+' '
L_str += 'A '+str(R)+' '+str(R)+' 0 0 0 '+str(240-S+R)+' '+str(240+S)+' '
L_str += 'L '+str(360-S-R)+' '+str(240+S)+' '
L_str += 'A '+str(R)+' '+str(R)+' 0 0 1 '+str(360-S)+' '+str(240+S+R)+' '
L_str += 'L '+str(360-S)+' '+str(360-S-R)+' '
L_str += 'A '+str(R)+' '+str(R)+' 0 0 1 '+str(360-S-R)+' '+str(360-S)+' '
L_str += 'L '+str(120+S+R)+' '+str(360-S)+' '
L_str += 'A '+str(R)+' '+str(R)+' 0 0 1 '+str(120+S)+' '+str(360-S-R)+' '
L_str += 'L '+str(120+S)+' '+str(S+R)+' '
L_str += 'A '+str(R)+' '+str(R)+' 0 0 1 '+str(120+S+R)+' '+str(S)+' '
L_str += 'Z" style="fill:#FF0000; stroke:#000000; stroke-width:10"/>\n'
L_str += '</svg>\n'

L_file = open("L_piece.svg",'w')
L_file.write(L_str)
L_file.close()
