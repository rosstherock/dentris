﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blinking_script : MonoBehaviour {

	[SerializeField] private GameObject eyes;
	[SerializeField] private Sprite eyes_open_sprite;
	[SerializeField] private Sprite eyes_closed_sprite;

	public bool blink = false;
	public bool can_blink = true;
	private SpriteRenderer sr;

	void Start() {
		sr = eyes.GetComponent<SpriteRenderer> ();
	}

	void Update () {
		if (can_blink) {
			if (blink) 
			{
				// if we currently have closed eyes, then there is a 0.1 change (10%) that the eyes will open this frame.
				if (Random.Range(0.0f,1.0f) < 0.1f) {
					blink = false;
					sr.sprite = eyes_open_sprite;
				}
			} 
			else 
			{
				// if we currently have open eyes, then there is a 0.01 change (1%) that the eyes will close this frame.
				if (Random.Range(0.0f,1.0f) < 0.01f) {
					blink = true;
					sr.sprite = eyes_closed_sprite;
				}
			}
		}
	}

}
