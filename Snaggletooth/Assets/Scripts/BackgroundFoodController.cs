﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundFoodController : MonoBehaviour {

	public float center;
	private Vector3 speed;
	private float angular_velocity;

	[SerializeField] float x_max_velocity;
	[SerializeField] float y_min_velocity;
	[SerializeField] float y_max_velocity;
	[SerializeField] float max_angular_velocity;
	[SerializeField] float size_min;
	[SerializeField] float size_max;


	void Start () {
		rerandomise ();
	}

	void Update() 
	{
		if (transform.position.y < -7.0f) {
			rerandomise ();
		} else {
			transform.position += Time.deltaTime * speed;
			transform.Rotate (0.0f, 0.0f, Time.deltaTime * angular_velocity);
		}
	}

	private void rerandomise() {
		float vy = y_min_velocity + Random.value * (y_max_velocity - y_min_velocity);
		float vx = -x_max_velocity + Random.value * 2.0f * x_max_velocity;
		speed = new Vector3 (vx, vy);
		angular_velocity = Random.Range (-max_angular_velocity, max_angular_velocity);
		float size = size_min + Random.value * (size_max - size_min);
		transform.localScale = new Vector3 (size, size, size);
		float x = center + Random.value - 0.5f;
		float y = 0;
		while (y < 7.0f || Random.value > 0.5) {
			x -= vx;
			y -= vy;
		}
		transform.position = new Vector3 (x, y);
	}
}
