﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LogoSceneController : MonoBehaviour {


	void Start()
	{
		StartCoroutine(WaitThenLoad());
	}

	IEnumerator WaitThenLoad()
	{
		yield return new WaitForSeconds(3);
		SceneManager.LoadScene ("MenuScene");
	}
}
