﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingsSceneControl : MonoBehaviour {

	[SerializeField] private Camera the_camera;

	[SerializeField] private GameObject confirmPanel;
	[SerializeField] private Text confirmPanel_titleText;

	[SerializeField] private Text titleText;

	[SerializeField] private GameObject settingsPanel;
	[SerializeField] private Button resetProgressButton;
	[SerializeField] private Button resetHardcoreProgressButton;
	[SerializeField] private Button soundVolumeButton;
	[SerializeField] private Text soundVolumeButtonText;
	[SerializeField] private Button gridButton;
	[SerializeField] private Text gridButtonText;
	[SerializeField] private Button backButton;

	[SerializeField] private GameObject prefab_banana;
	private GameObject[] background_object;

	void Start () 
	{
		if (MainControl.normal_progress.Substring (1, 1) == "U") {
			resetProgressButton.gameObject.SetActive (false);
		} else {
			resetProgressButton.gameObject.SetActive (true);
		}
		if (MainControl.hardcore_progress.Substring (1, 1) == "U") {
			resetHardcoreProgressButton.gameObject.SetActive (false);
		} else {
			resetHardcoreProgressButton.gameObject.SetActive (true);
		}
		if (MainControl.background_grid == 1) {
			gridButtonText.text = "Turn Grid Off";
		} else {
			gridButtonText.text = "Turn Grid On";
		}
		if (MainControl.sound_volume == 70) {
			soundVolumeButtonText.text = "Turn Sound Off";
		} else {
			soundVolumeButtonText.text = "Turn Sound On";
		}
		the_camera.backgroundColor = new Color (135.0f/255.0f, 206.0f/255.0f, 236.0f/255.0f);
		background_object = new GameObject[11];
		for (int i = 0; i < 11; i++) {
			background_object[i] = (GameObject)Instantiate(prefab_banana);
			background_object[i].GetComponent<BackgroundFoodController> ().center = -12.5f + 2.5f * i;
		}

	} // END Start() 
	
	public void resetProgressButtonClicked() 
	{
		MainControl.hardcore_mode = false;
		confirmPanel_titleText.text = "RESET PROGRESS?";
		titleText.gameObject.SetActive (false);
		settingsPanel.SetActive (false);
		confirmPanel.SetActive (true);
	}

	public void resetHardcoreProgressButtonClicked() 
	{
		MainControl.hardcore_mode = true;
		confirmPanel_titleText.text = "RESET HARDCORE PROGRESS?";
		titleText.gameObject.SetActive (false);
		settingsPanel.SetActive (false);
		confirmPanel.SetActive (true);
	}

	public void YesButtonClicked()
	{
		if (MainControl.hardcore_mode) {
			MainControl.restartHardcoreProgress ();
			resetHardcoreProgressButton.gameObject.SetActive (false);
		} else {
			MainControl.restartNormalProgress ();
			resetProgressButton.gameObject.SetActive(false);
		}
		titleText.gameObject.SetActive (true);
		settingsPanel.SetActive (true);
		confirmPanel.SetActive (false);
	}

	public void NoButtonClicked() 
	{
		titleText.gameObject.SetActive (true);
		settingsPanel.SetActive (true);
		confirmPanel.SetActive (false);
	}

	public void GridButtonClicked() 
	{
		if (MainControl.background_grid == 1) {
			MainControl.setBackgroundGrid (false);
			gridButtonText.text = "Turn Grid On";
		} else {
			MainControl.setBackgroundGrid (true);
			gridButtonText.text = "Turn Grid Off";
		}
	}

	public void SoundVolumeButtonClicked() 
	{
		if (MainControl.sound_volume == 70) {
			MainControl.setSoundVolume (0);
			soundVolumeButtonText.text = "Turn Sound On";
		} else {
			MainControl.setSoundVolume (70);
			soundVolumeButtonText.text = "Turn Sound Off";
		}
		Debug.Log (MainControl.sound_volume);
	}

	public void backButtonClicked()
	{
		SceneManager.LoadScene ("MenuScene");
	}

}
