﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class MenuSceneController : MonoBehaviour 
{
	[SerializeField] private Camera the_camera;
	[SerializeField] private Text playText;
	[SerializeField] private Button quitButton;

	[SerializeField] private GameObject prefab_cloud;
	private GameObject[] background_object;

	private void SetTheBackground() 
	{
		if (MainControl.normal_progress.Substring (1, 1) == "A") {
			background_object = new GameObject[3];
			for (int i = 0; i < 3; i++) {
				background_object[i] = (GameObject)Instantiate (prefab_cloud);
				background_object[i].GetComponent<CloudController> ().y_center = -3.0f + 3.0f*i;
			}
		}
	}

	void Start () 
	{
		SetTheBackground ();
		if (MainControl.normal_progress.Substring (1, 1) == "U") {
			playText.text = "New Game";
		} else {
			playText.text = "Continue";
		}
		/*
		#if UNITY_WEBGL
		quitButton.gameObject.SetActive(false);
		#endif
		*/
	}

	public void playGameClicked() {
		MainControl.hardcore_mode = false;
		MainControl.current_level = 1;
		if (MainControl.normal_progress.Substring (1, 1) == "A") {
			SceneManager.LoadScene ("SelectScene");
		} else {
			SceneManager.LoadScene ("PuzzleScene");
		}
	}

	public void hardcoreClicked() {
		MainControl.hardcore_mode = true;
		MainControl.current_level = 1;
		SceneManager.LoadScene ("SelectScene");
	}

	public void settingsClicked() {
		SceneManager.LoadScene ("SettingsScene");
	}

	public void quit_game()
	{
		#if (UNITY_EDITOR)
		UnityEditor.EditorApplication.isPlaying = false;
		#elif (UNITY_STANDALONE) 
		Application.Quit();
		#elif (UNITY_WEBGL)
		Application.OpenURL("http://www.gustygames.com/index");
		#endif
	}

}
