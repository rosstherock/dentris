﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class PuzzleSceneControl : MonoBehaviour 
{
	[SerializeField] private Camera the_camera;
	[SerializeField] private GameObject background_music;
	[SerializeField] private GameObject win_sound;
	[SerializeField] private GameObject lose_sound;
	[SerializeField] private GameObject pluck_C4;
	[SerializeField] private GameObject pluck_D4;
	[SerializeField] private GameObject pluck_E4;
	[SerializeField] private GameObject pluck_F4;
	[SerializeField] private GameObject pluck_G4;
	[SerializeField] private GameObject pluck_A5;
	[SerializeField] private GameObject pluck_B5;
	[SerializeField] private GameObject pluck_C5;
	[SerializeField] private GameObject pluck_D5;
	[SerializeField] private GameObject pluck_E5;
	[SerializeField] private GameObject pluck_F5;
	[SerializeField] private GameObject pluck_G5;
	[SerializeField] private GameObject pluck_A6;
	[SerializeField] private GameObject pluck_B6;
	[SerializeField] private GameObject pluck_C6;

	[SerializeField] private GameObject puzzle_panel;
	[SerializeField] private Text puzzlepanel_text;
	[SerializeField] private Button puzzlepanel_undoButton;
	[SerializeField] private Button puzzlepanel_restartButton;
	[SerializeField] private Text levelnumber_text;

	[SerializeField] private GameObject finish_panel;
	[SerializeField] private Text finish_text;
	[SerializeField] private Button finishpanel_undoButton;
	[SerializeField] private Button finishpanel_restartButton;
	[SerializeField] private Button finishpanel_nextLevelButton;
	[SerializeField] private Button finishpanel_selectButton;
	[SerializeField] private Text finishpanel_nextLevelText;
	[SerializeField] private GameObject finishpanel_star;

	[SerializeField] private GameObject backgroundGrid;
	[SerializeField] private GameObject target;
	[SerializeField] private GameObject dragging_target;

	[SerializeField] private GameObject prefab_Tooth;
	[SerializeField] private GameObject prefab_B;
	[SerializeField] private GameObject prefab_L;
	[SerializeField] private GameObject prefab_O;
	[SerializeField] private GameObject prefab_S;
	[SerializeField] private GameObject prefab_T;

	public bool can_make_a_move; // false when a piece is moving or when the finish_panel is Active.
	private string starting_code; // <code> contains information about which pieces exist and where they start.
	private string starting_state; // <state>  only contains infomation about the piece locations.
	private string[] history = new string[40]; // this is an array of <state>s
	private int piece_count; // total number of pieces (including the tooth and any B-pieces
	private GameObject[] piece;
	private int move_count; 

	private string get_current_state() {
		int piece_count = piece.Length;
		string state = "";
		for (int i = 0; i < piece_count; i++) 
		{
			state +=  (Mathf.RoundToInt(piece[i].transform.position.x)+5).ToString ();
			state +=  (Mathf.RoundToInt(piece[i].transform.position.y)+5).ToString ();
		}
		return state;
	}

	private void set_current_state(string state) {
		for (int i = 0; i < piece_count; i++) {
			if (starting_code.Substring (5 * i, 1) != "B") {
				piece [i].GetComponent<RockController> ().halt ();
			}
			int x = int.Parse (state.Substring (2*i, 1))-5;
			int y = int.Parse (state.Substring (2*i+1, 1))-5;
			piece[i].transform.position = new Vector3(x, y, 0);
		}
		win_sound.SetActive (false);
		lose_sound.SetActive (false);
	}

	private void SetTheGrid() {
		if (MainControl.background_grid == 1) {
			backgroundGrid.SetActive (true);
		} else { 
			backgroundGrid.SetActive (false);
		}
	}

	private void SetThePuzzlePanelText() {
		if (MainControl.current_level == 1 && !MainControl.hardcore_mode) { 
			puzzlepanel_text.text = "Drag the Tooth";
			puzzlepanel_text.gameObject.SetActive(true);
		} else if (MainControl.current_level == 3 && !MainControl.hardcore_mode) { 
			puzzlepanel_text.text = "Drag the Square";
			puzzlepanel_text.gameObject.SetActive(true);
		} else {
			puzzlepanel_text.gameObject.SetActive(false);
		}
	}

	void Start () { 
		can_make_a_move = false; 
		SetTheGrid (); 
		if (MainControl.hardcore_mode) { 
			SetTheBoard (MainControl.hardcore_puzzle [MainControl.current_level].code); 
		} else { 
			SetTheBoard (MainControl.normal_puzzle [MainControl.current_level].code); 
		} 
	} 

	private void SetTheBoard(string new_starting_code) 
	{ 
		can_make_a_move = false; 
		starting_code = new_starting_code; 
		starting_state = ""; 
		piece_count = starting_code.Length/5; 
		if (piece != null) { 
			for (int i = 0; i < piece.Length; i++) { 
				Destroy(piece[i]); 
			} 
		} 
		piece = new GameObject[piece_count];
		for (int i = 0; i < piece_count; i++) 
		{
			starting_state += starting_code.Substring (5 * i + 3, 2);
			string piece_type = starting_code.Substring (5 * i, 1);
			int r = int.Parse(starting_code.Substring (5 * i+1, 1));
			int x = int.Parse(starting_code.Substring (5 * i+3, 1))-5;
			int y = int.Parse(starting_code.Substring (5 * i+4, 1))-5;
			if (piece_type == "t") {
				piece[i] = (GameObject)Instantiate(prefab_Tooth);
				piece[i].transform.localScale = new Vector3(1, 1, 1);
				piece[i].transform.position = new Vector3(x, y, 0);
				piece[i].GetComponent<RockController> ().setPSC (this,"t",0,false);
			} else if (piece_type == "B") {
				piece[i] = (GameObject)Instantiate (prefab_B);
				piece[i].transform.localScale = new Vector3(1, 1, 1);
				piece[i].transform.position = new Vector3(x, y, 0);
			} else {
				if (piece_type == "L") {
					piece[i] = (GameObject)Instantiate (prefab_L);
				} else if (piece_type == "O") {
					piece[i] = (GameObject)Instantiate(prefab_O);
				} else if (piece_type == "S") {
					piece[i] = (GameObject)Instantiate(prefab_S);
				} else {
					piece[i] = (GameObject)Instantiate(prefab_T);
				} 
				//piece[i].transform.localScale = new Vector3(1, 1, 1);
				for (int j = 0; j < r; j++) {
					piece[i].transform.Rotate (Vector3.forward * -90);
				}
				bool flip = starting_code.Substring (5 * i + 2, 1) == "F";
				if (flip) {
					piece[i].transform.localScale = new Vector3(-1, 1, 1);
				} else {
					piece[i].transform.localScale = new Vector3(1, 1, 1);
				}
				piece[i].transform.position = new Vector3(x, y, 0);
				piece[i].GetComponent<RockController> ().setPSC (this,piece_type,r,flip);
			} 
		}
		history = new string[40];
		for (int i = 0; i < 40; i++) {
			history [i] = starting_state;
		}
		move_count = 0;
		puzzlepanel_undoButton.gameObject.SetActive (false);
		puzzlepanel_restartButton.gameObject.SetActive (false);
		piece [0].SetActive (true);
		target.SetActive (true);
		dragging_target.SetActive (false);
		dragging_target.transform.position = new Vector3 (4.8f, -0.1f);
		puzzle_panel.SetActive (true);
		finish_panel.SetActive (false);
		can_make_a_move = true;
		if (MainControl.current_level == 1 && !MainControl.hardcore_mode) {
			puzzlepanel_text.gameObject.SetActive (true);
		} else {
			puzzlepanel_text.gameObject.SetActive (false);
		} 
		win_sound.SetActive (false);
		lose_sound.SetActive (false);
		levelnumber_text.text = "LEVEL\n"+MainControl.current_level.ToString ();
	}


	public void something_bumped (bool win) 
	{
		string cs = get_current_state ();
		if (history [0] != cs) {
			if (MainControl.sound_volume != 0 && !win) {
				pluck_C4.SetActive (false);
				pluck_D4.SetActive (false);
				pluck_E4.SetActive (false);
				pluck_F4.SetActive (false);
				pluck_G4.SetActive (false);
				pluck_A5.SetActive (false);
				pluck_B5.SetActive (false);
				pluck_C5.SetActive (false);
				pluck_D5.SetActive (false);
				pluck_E5.SetActive (false);
				pluck_F5.SetActive (false);
				pluck_G5.SetActive (false);
				pluck_A6.SetActive (false);
				pluck_B6.SetActive (false);
				pluck_C6.SetActive (false);
				int s = Random.Range (0,15);
				if (s==0) {
					pluck_C4.SetActive (true);
				} else if (s==1) {
					pluck_D4.SetActive (true);
				} else if (s==2) {
					pluck_E4.SetActive (true);
				} else if (s==3) {
					pluck_F4.SetActive (true);
				} else if (s==4) {
					pluck_G4.SetActive (true);
				} else if (s==5) {
					pluck_A5.SetActive (true);
				} else if (s==6) {
					pluck_B5.SetActive (true);
				} else if (s==7) {
					pluck_C5.SetActive (true);
				} else if (s==8) {
					pluck_D5.SetActive (true);
				} else if (s==9) {
					pluck_E5.SetActive (true);
				} else if (s==10) {
					pluck_F5.SetActive (true);
				} else if (s==11) {
					pluck_G5.SetActive (true);
				} else if (s==12) {
					pluck_A6.SetActive (true);
				} else if (s==13) {
					pluck_B6.SetActive (true);
				} else if (s==14) {
					pluck_C6.SetActive (true);
				} else {
					Debug.Log("No Sound Effect! Why?");
				}
			} 

			move_count += 1;
			for (int i = move_count; i > 0; i--) {
				history [i] = history[i-1];
			}
			history [0] = cs;
			puzzlepanel_undoButton.gameObject.SetActive (true);
			puzzlepanel_restartButton.gameObject.SetActive (true);
		}
		this.can_make_a_move = true;
	}
		
	public void undo() 
	{
		if (move_count > 0 && can_make_a_move) {
			for (int i = 0; i < move_count && i <= 38; i++) {
				history [i] = history [i + 1];
			}
			move_count -= 1;
			set_current_state (history [0]);
			if (move_count == 0) {
				puzzlepanel_undoButton.gameObject.SetActive (false);
				puzzlepanel_restartButton.gameObject.SetActive (false);
			}
		} else if (move_count > 0 && !can_make_a_move) {
			set_current_state (history [0]);
			if (move_count == 0) {
				puzzlepanel_undoButton.gameObject.SetActive (false);
				puzzlepanel_restartButton.gameObject.SetActive (false);
			}
		} else {
			restart ();
		}
		piece [0].SetActive (true);
		target.SetActive (true);
		dragging_target.SetActive (false);
		dragging_target.transform.position = new Vector3 (4.8f, -0.1f);
		puzzle_panel.SetActive (true);
		finish_panel.SetActive (false);
		this.can_make_a_move = true;
		for (int i = 1; i < piece_count; i++) {
			piece [i].SetActive (true);
		}
	}

	public void restart() 
	{
		history = new string[40];
		for (int i = 0; i < 40; i++) {
			history [i] = starting_state;
		}
		move_count = 0;
		set_current_state (starting_state);
		puzzlepanel_undoButton.gameObject.SetActive (false);
		puzzlepanel_restartButton.gameObject.SetActive (false);
		piece [0].SetActive (true);
		target.SetActive (true);
		dragging_target.SetActive (false);
		dragging_target.transform.position = new Vector3 (4.8f, -0.1f);
		puzzle_panel.SetActive (true);
		finish_panel.SetActive (false);
		this.can_make_a_move = true;
		for (int i = 1; i < piece_count; i++) {
			piece [i].SetActive (true);
		}
	}

	public void next_level()
	{
		MainControl.unlock_level(MainControl.current_level);
		if (MainControl.current_level < 36) {
			MainControl.current_level += 1;
			if (MainControl.hardcore_mode) {
				SetTheBoard (MainControl.hardcore_puzzle [MainControl.current_level].code);
			} else {
				SetTheBoard (MainControl.normal_puzzle [MainControl.current_level].code);
				SetThePuzzlePanelText ();
			}
		} else {
			SceneManager.LoadScene ("SelectScene");
		}
	}

	public void load_selectScene() 
	{
		SceneManager.LoadScene ("SelectScene");
	}

	public void solvePuzzle()
	{
		this.can_make_a_move = false;
		pluck_C4.SetActive (false);
		pluck_D4.SetActive (false);
		pluck_E4.SetActive (false);
		pluck_F4.SetActive (false);
		pluck_G4.SetActive (false);
		pluck_A5.SetActive (false);
		pluck_B5.SetActive (false);
		pluck_C5.SetActive (false);
		pluck_D5.SetActive (false);
		pluck_E5.SetActive (false);
		pluck_F5.SetActive (false);
		pluck_G5.SetActive (false);
		pluck_A6.SetActive (false);
		pluck_B6.SetActive (false);
		pluck_C6.SetActive (false);
		if (MainControl.sound_volume > 0) {
			win_sound.SetActive (true);
		}
		bool star_finish = MainControl.solveLevel (MainControl.current_level, move_count);
		if (star_finish) {
			finishpanel_star.SetActive (true);
		} else {
			finishpanel_star.SetActive (false);
		} 
		finishpanel_undoButton.gameObject.SetActive (false);
		finishpanel_selectButton.gameObject.SetActive (true);
		finishpanel_nextLevelText.text = "NEXT";
		finishpanel_nextLevelButton.gameObject.SetActive (true);
		target.SetActive (false);
		piece [0].SetActive (false);
		dragging_target.SetActive (true);

		puzzle_panel.SetActive (false);
		StartCoroutine(showWinPanelAfterDelay());
	}

	private IEnumerator showWinPanelAfterDelay() {
		yield return new WaitForSeconds(1);
		for (int i = 1; i < piece_count; i++) {
			piece [i].SetActive (false);
		}
		finish_panel.SetActive (true);
		finish_text.text = "You Win!";
	}

	public void failPuzzle() {
		if (MainControl.sound_volume > 0) {
			lose_sound.SetActive (true);
		}
		finishpanel_star.SetActive (false);
		this.can_make_a_move = false;
		if ( (MainControl.current_level<36 && MainControl.normal_progress.Substring(5,1) != "L") || MainControl.hardcore_mode) {
			finishpanel_nextLevelText.text = "SKIP";
			finishpanel_nextLevelButton.gameObject.SetActive (true);
		} else {
			finishpanel_nextLevelButton.gameObject.SetActive (false);
		}
		finishpanel_undoButton.gameObject.SetActive (true);
		finishpanel_selectButton.gameObject.SetActive (false);
		puzzle_panel.SetActive (false);
		finish_panel.SetActive (true);
		finish_text.text = "You Lose";
	}

}


