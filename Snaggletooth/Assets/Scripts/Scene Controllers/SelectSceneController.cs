﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class SelectSceneController : MonoBehaviour 
{
	[SerializeField] private Button leftButton;
	[SerializeField] private Button rightButton;

	[SerializeField] private GameObject puzzle_panels;
	[SerializeField] private GameObject panel_1;
	[SerializeField] private GameObject panel_2;
	[SerializeField] private GameObject panel_3;
	[SerializeField] private GameObject prefabLevelButton;

	private Animator animator;
	private int active_panel=1,max_unlocked_panel=1;

	void Start () 
	{
		animator = puzzle_panels.GetComponent<Animator>();

		float s = Mathf.Min(Screen.width / 640.0f , Screen.height / 393.0f);
		puzzle_panels.transform.localScale = new Vector3 (s, s, s);
		// panel_1.transform.localScale = new Vector3 (s, s, s);
		// panel_2.transform.localScale = new Vector3 (s, s, s);
		// panel_3.transform.localScale = new Vector3 (s, s, s);

		string state;
		Button b;
		Button[] button_list_1 = panel_1.GetComponentsInChildren<Button> ();
		Button[] button_list_2 = panel_2.GetComponentsInChildren<Button> ();
		Button[] button_list_3 = panel_3.GetComponentsInChildren<Button> ();
		for (int i = 1; i <= 36; i++) {
			if (i <= 12) {
				b = button_list_1 [i - 1];
			} else if (i <= 24) {
				b = button_list_2 [i - 13];
			} else {
				b = button_list_3 [i - 25];
			}
			if (MainControl.hardcore_mode) {
				state = MainControl.hardcore_progress.Substring (i, 1);
			} else {
				state = MainControl.normal_progress.Substring (i, 1);
			}

			if (state=="L") {
				b.gameObject.SetActive (false);
			} else {
				b.gameObject.SetActive (true);
				if (state == "A") { 
					b.GetComponent<Image> ().color = new Color (1.0f, 1.0f, 1.0f);
				} else if (state == "B") {
					b.GetComponent<Image> ().color = new Color (1.0f, 1.0f, 0.85f);
				} else { // state="U"
					b.GetComponent<Image> ().color = new Color (0.85f, 0.85f, 0.6f);
				}
				max_unlocked_panel = (i + 11) / 12;
			}

		}

		if (MainControl.current_level > 24) {
			active_panel = 3;
		} else if (MainControl.current_level > 12) {
			active_panel = 2;
		} else {
			active_panel = 1;
		}
		left_right_buttons_SetActive ();

	} // END Start() 

	public void LevelButtonClicked(int level_index) 
	{
		MainControl.current_level = level_index;
		SceneManager.LoadScene ("PuzzleScene");
	}

	public void right_clicked()
	{
		active_panel += 1;
		if (active_panel > max_unlocked_panel) active_panel = max_unlocked_panel;
		left_right_buttons_SetActive ();
	}

	public void left_clicked()
	{
		active_panel -= 1;
		if (active_panel < 1) active_panel = 1;
		left_right_buttons_SetActive ();
	}

	private void left_right_buttons_SetActive() 
	{
		animator.SetInteger("active_panel",active_panel);
		if (active_panel == 1) {
			leftButton.gameObject.SetActive (false);
		} else {
			leftButton.gameObject.SetActive (true);
		}
		if (active_panel < max_unlocked_panel) {
			rightButton.gameObject.SetActive (true);
		} else {
			rightButton.gameObject.SetActive (false);
		}
	}

	public void backToMenuButtonClicked() 
	{
		MainControl.current_level = 1;
		SceneManager.LoadScene ("MenuScene");
	}
}








/*
		float sx = (Screen.width / Screen.dpi) * 25.4f;
		float sy = (Screen.height / Screen.dpi) * 25.4f;
		float cell_size = Mathf.Min ( (sx-40.0f)/4.5f , sy/3.4f );

		rt1.sizeDelta = new Vector2 (4.3f*cell_size,3.2f*cell_size);
		rt2.sizeDelta = new Vector2 (4.3f*cell_size,3.2f*cell_size);
		rt3.sizeDelta = new Vector2 (4.3f*cell_size,3.2f*cell_size);
		*/



/******** old start() *****************
 * 
 * for(int i = 1; i < 37; i++)
		{
			int pan_idx = (i-1) / 12;
			int row_idx = ((i - 1) / 4) % 3;
			int col_idx = (i-1) % 4;

			GameObject goButton = (GameObject)Instantiate(prefabLevelButton);
			goButton.GetComponentInChildren<Text> ().fontSize = (int)(0.3*cell_size);
			if (pan_idx==0) {
				goButton.transform.SetParent (rt1, false);
			} else if (pan_idx==1) {
				goButton.transform.SetParent (rt2, false);
			} else if (pan_idx==2) {
				goButton.transform.SetParent (rt3, false);
			} 
			//goButton.GetComponent<RectTransform>().sizeDelta = new Vector2 (cell_size,cell_size);
			goButton.transform.localPosition = new Vector3 ((0.1f+col_idx)*cell_size,(0.1f+2-row_idx)*cell_size);

			Button tempButton = goButton.GetComponent<Button>();
			int tempInt = i;
			tempButton.onClick.AddListener(() => LevelButtonClicked(tempInt));
			goButton.GetComponentInChildren<Text> ().text = tempInt.ToString ();

			string state = "L";
			if (MainControl.hardcore_mode) {
				state = MainControl.hardcore_progress.Substring (i, 1);
			} else {
				state = MainControl.normal_progress.Substring (i, 1);
			}
			if (state == "L") {
				goButton.SetActive (false);
			} else {
				goButton.SetActive (true);
				max_unlocked_panel = Mathf.CeilToInt((1.0f*i)/12.0f);
				if (state == "A") { 
					goButton.GetComponent<Image> ().color = new Color (1.0f, 1.0f, 1.0f);
				} else if (state == "B") {
					goButton.GetComponent<Image> ().color = new Color (1.0f, 1.0f, 0.85f);
				} else { // state="U"
					goButton.GetComponent<Image> ().color = new Color (0.85f, 0.85f, 0.6f);
				}
			}
		}
*******************************************/