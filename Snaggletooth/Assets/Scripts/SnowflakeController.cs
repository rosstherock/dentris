﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowflakeController : MonoBehaviour {

	public float center;
	private Vector3 speed;
	private float angular_velocity;

	void Start () {
		rerandomise ();
	}

	void Update() 
	{
		if (transform.position.y < -7.0f) {
			rerandomise ();
		} else {
			transform.position += Time.deltaTime * speed;
			transform.Rotate (0.0f, 0.0f, Time.deltaTime * angular_velocity);
		}
	}

	private void rerandomise() {
		float vy = Random.Range (-1.8f, -0.8f);
		float vx = Random.Range (-0.5f, 0.5f);
		speed = new Vector3 (vx, vy);
		angular_velocity = Random.Range (-150.0f, 150.0f);
		float size = Random.Range (0.4f, 1.0f);
		transform.localScale = new Vector3 (size, size, size);
		float x = center + Random.Range (-0.5f, 0.5f);
		float y = 0;
		while (y < 7.0f) {
			x -= vx;
			y -= vy;
		}
		transform.position = new Vector3 (x, y);
	}
}
