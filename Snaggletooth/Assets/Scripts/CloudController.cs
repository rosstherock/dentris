﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudController : MonoBehaviour {

	[SerializeField] float speed;
	[SerializeField] GameObject atom_A;
	[SerializeField] GameObject atom_B;
	[SerializeField] GameObject atom_C;
	[SerializeField] GameObject atom_D;
	[SerializeField] GameObject atom_E;
	[SerializeField] GameObject atom_F;

	private Transform t,a,b,c,d,e,f;
	private Vector3 displacement;
	public float y_center;

	void Start () {
		t = GetComponent<Transform> ();
		a = atom_A.GetComponent<Transform> ();
		b = atom_B.GetComponent<Transform> ();
		c = atom_C.GetComponent<Transform> ();
		d = atom_D.GetComponent<Transform> ();
		e = atom_E.GetComponent<Transform> ();
		f = atom_F.GetComponent<Transform> ();
		rerandomise ();
		t.position = new Vector3 (Random.Range(-18.0f,-14.0f), y_center, 0.0f);
	}
		
	void FixedUpdate() 
	{
		if (t.position.x > 14.0f) {
			rerandomise ();
			t.position = new Vector3 (-14.0f, y_center, 0.0f);
		} else {
			t.position = t.position + displacement;
		}
	}



	private void rerandomise() {
		a.localScale = new Vector3(Random.Range(0.9f,1.2f),Random.Range(0.8f,1.0f),1.0f);
		a.localPosition = new Vector3(Random.Range(-1.4f,-1.2f),Random.Range(-0.1f,0.1f),1.0f);

		b.localScale = new Vector3(Random.Range(0.8f,1.0f),Random.Range(0.8f,0.9f),1.0f);
		b.localPosition = new Vector3(Random.Range(-1.1f,-0.9f),Random.Range(0.9f,1.1f),1.0f);

		c.localScale = new Vector3(Random.Range(0.8f,1.2f),Random.Range(0.8f,1.1f),1.0f);
		c.localPosition = new Vector3(Random.Range(-0.1f,0.1f),Random.Range(-0.1f,0.1f),1.0f);

		d.localScale = new Vector3(Random.Range(0.9f,1.1f),Random.Range(0.9f,1.0f),1.0f);
		d.localPosition = new Vector3(Random.Range(-0.1f,0.1f),Random.Range(1.0f,1.2f),1.0f);

		e.localScale = new Vector3(Random.Range(0.9f,1.2f),Random.Range(0.8f,1.1f),1.0f);
		e.localPosition = new Vector3(Random.Range(1.2f,1.4f),Random.Range(-0.1f,0.1f),1.0f);

		f.localScale = new Vector3(Random.Range(0.8f,1.0f),Random.Range(0.8f,0.9f),1.0f);
		f.localPosition = new Vector3(Random.Range(0.9f,1.1f),Random.Range(0.9f,1.1f),1.0f);

		t.localScale = new Vector3 (Random.Range (0.7f, 0.9f), Random.Range (0.7f, 0.9f));
		displacement = new Vector3 (Random.Range(speed*0.5f,speed*1.5f) , 0.0f, 0.0f);
	}

}
