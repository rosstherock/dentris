﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public struct Puzzle
{
	public Puzzle(string _code, int min_number_of_moves)
	{
		this.code = _code;
		this.min_moves = min_number_of_moves;
	}

	public string code { get; private set; }
	public int min_moves { get; private set; }
}



public class MainControl : MonoBehaviour {

	public static MainControl mainControl;

	public static string normal_progress, hardcore_progress;
	public static int current_level;
	public static bool hardcore_mode;

	public static int background_grid;
	public static int sound_volume;

	public static List<Puzzle> normal_puzzle = new List<Puzzle>();
	public static List<Puzzle> hardcore_puzzle = new List<Puzzle>();

	void Start () 
	{
		normal_puzzle = new List<Puzzle>();
		normal_puzzle.Add (new Puzzle ("t0N56L0N45", 0)); // puzzle 0 is never used for anything.

		normal_puzzle.Add (new Puzzle ("t0N26B0N56B0N43B0N64", 4)); // 1st level: introduce drag mechanic.
		normal_puzzle.Add (new Puzzle ("t0N26B0N56B0N44B0N85B0N73", 6)); // 2nd level: demonstrate that 'passing over' the target doesn't win.
		normal_puzzle.Add (new Puzzle ("t0N56B0N33O0N83",2)); // 3rd level: demonstrate that tetrominos can be dragged too. 
		normal_puzzle.Add (new Puzzle ("t0N38L0N33B0N74", 4)); // L-only.

		normal_puzzle.Add (new Puzzle ("t0N73L2N85T3N36", 4)); // LT.
		normal_puzzle.Add (new Puzzle ("t0N23S1N74L3N36B0N78", 4)); // SL.
		normal_puzzle.Add (new Puzzle ("t0N37L0N34B0N77B0N65", 6)); // L-only.
		normal_puzzle.Add (new Puzzle ("t0N65S1F57O0N37", 4)); // SO.

		normal_puzzle.Add (new Puzzle ("t0N28O0N57B0N35B0N74B0N86",9)); // O-only level.
		normal_puzzle.Add (new Puzzle ("t0N33S0F36L2N67", 4)); // SL.
		normal_puzzle.Add (new Puzzle ("t0N32L3N72O0N67", 4)); // LO.
		normal_puzzle.Add (new Puzzle ("t0N37S1F77L3N74B0N67", 5)); // SL.

		// 12 puzzles before here.

		normal_puzzle.Add (new Puzzle ("t0N43T0N73B0N52B0N57B0N35", 7)); // T-only level. 
		normal_puzzle.Add (new Puzzle ("t0N52L3F64T0N77", 4)); // LT.
		normal_puzzle.Add (new Puzzle ("t0N44S1N37L3F77", 5)); // (puzzle number 13?)
		normal_puzzle.Add (new Puzzle ("t0N45L3F78O0N83", 5)); // LO.

		normal_puzzle.Add (new Puzzle ("t0N66S1N63O0N46B0N23", 5)); // 
		normal_puzzle.Add (new Puzzle ("t0N84S0F53O0N76B0N25", 5)); // 
		normal_puzzle.Add (new Puzzle ("t0N32S1F63T1N35B0N68", 4)); // 
		normal_puzzle.Add (new Puzzle ("t0N23L0F87O0N47B0N53", 6)); 

		normal_puzzle.Add (new Puzzle ("t0N48S0F45T1N87B0N34", 6)); // 
		normal_puzzle.Add (new Puzzle ("t0N67S0N35T3N33B0N63", 6)); // 
		normal_puzzle.Add (new Puzzle ("t0N63L0F45O0N75B0N37", 5)); // 
		normal_puzzle.Add (new Puzzle ("t0N47L1F63T3N77B0N68", 5)); // 

		// 24 puzzle before here

		normal_puzzle.Add (new Puzzle ("t0N62L1F56T1N86B0N53", 4)); // 
		normal_puzzle.Add (new Puzzle ("t0N87L2F73T1N57B0N26", 5)); // 
		normal_puzzle.Add (new Puzzle ("t0N74L3F57T1N54B0N34", 5)); // 
		normal_puzzle.Add (new Puzzle ("t0N32L3F68T1N64B0N26", 5)); // 

		normal_puzzle.Add (new Puzzle ("t0N74O0N67T3N53B0N35", 5)); // 
		normal_puzzle.Add (new Puzzle ("t0N85O0N64T0N36B0N68", 4)); // 
		normal_puzzle.Add (new Puzzle ("t0N82O0N86T1N63B0N35", 5)); // 
		normal_puzzle.Add (new Puzzle ("t0N68S1N47L1N73B0N86B0N45", 5)); // great puzzle.

		normal_puzzle.Add (new Puzzle ("t0N56S0F42L0F83", 5)); // 
		normal_puzzle.Add (new Puzzle ("t0N57S0N74L1F42B0N46B0N77", 5)); // great puzzle.
		normal_puzzle.Add (new Puzzle ("t0N36S1F66T2N62", 5)); // 
		normal_puzzle.Add (new Puzzle ("t0N42S0F55L2F76B0N36B0N84", 10)); // awesome puzzle!



		// hardcore puzzles

		hardcore_puzzle = new List<Puzzle>();
		hardcore_puzzle.Add (new Puzzle ("t0N56L0N45", 0)); // puzzle 0 is never used for anything.

		hardcore_puzzle.Add (new Puzzle ("t0N88L1N73O0N37S1F57T3N65", 9)); // ok puzzle. Too easy for hardcore mode?
		hardcore_puzzle.Add (new Puzzle ("t0N44S0F62L0N27B0N24B0N88", 5)); // new puzzle.
		hardcore_puzzle.Add (new Puzzle ("t0N88L1N55O0N83T1N35", 7));
		hardcore_puzzle.Add (new Puzzle ("t0N42S1N67L1N44B0N74B0N37", 6)); // 

		hardcore_puzzle.Add (new Puzzle ("t0N34L0F74S1N37T2N42", 5)); // easy
		hardcore_puzzle.Add (new Puzzle ("t0N73L3N36T2N76S0F62O0N37", 6)); // good
		hardcore_puzzle.Add (new Puzzle ("t0N77S0F72O0N42T3N36", 7));
		hardcore_puzzle.Add (new Puzzle ("t0N82L2N63O0N87S1N35", 7)); // nice easy puzzle

		hardcore_puzzle.Add (new Puzzle ("t0N62L1F32T0N78S1N37", 7)); // good. I think this one is quite difficult?
		hardcore_puzzle.Add (new Puzzle ("t0N73L1N74O0N32T0N38S1F76", 5)); // good
		hardcore_puzzle.Add (new Puzzle ("t0N24L1N47O0N83T1N87", 7));
		hardcore_puzzle.Add (new Puzzle ("t0N24L3N42B0N28S1F77T3N26", 8)); // is this good?

		// 12 puzzles before here.

		hardcore_puzzle.Add (new Puzzle ("t0N62L2F73S1N25T1N67", 4)); 
		hardcore_puzzle.Add (new Puzzle ("t0N37L3N74O0N33S0N52T0N46", 5)); // easy?
		hardcore_puzzle.Add (new Puzzle ("t0N28S0N76L3N62T0N36", 6)); 
		hardcore_puzzle.Add (new Puzzle ("t0N58L0N54O0N32S1N27", 5)); 

		hardcore_puzzle.Add (new Puzzle ("t0N46L0N76O0N82T0N33", 6)); 
		hardcore_puzzle.Add (new Puzzle ("t0N54T3N26S1N75B0N87", 10)); // way too hard for-world-one
		hardcore_puzzle.Add (new Puzzle ("t0N86L1F32O0N82S0F57T1N54", 7)); 
		hardcore_puzzle.Add (new Puzzle ("t0N22L3F58O0N36S0F72T2N43", 7)); // awesome but really hard.

		hardcore_puzzle.Add (new Puzzle ("t0N82L2F57B0N27S1N36T2N42", 10)); // a good 3-piece + 1 B-block puzzle.
		hardcore_puzzle.Add (new Puzzle ("t0N62L1F32S0N47T0N78", 14)); // a good end-of-world-two puzzle?
		hardcore_puzzle.Add (new Puzzle ("t0N87L3N63O0N57S1F65T1N35", 11)); // good puzzle
		hardcore_puzzle.Add (new Puzzle ("t0N86O0N72L1F67S1N43T2N32", 10)); // awesome

		// 24 puzzles before here.

		hardcore_puzzle.Add (new Puzzle ("t0N23L0N37O0N57S0N52T1N77", 10)); // ok puzzle.
		hardcore_puzzle.Add (new Puzzle ("t0N77L1N47O0N85S0F32T0N35", 8)); // good puzzle.
		hardcore_puzzle.Add (new Puzzle ("t0N32L2F55B0N23S1N63T0N77", 10)); // super puzzle with a B-piece.
		hardcore_puzzle.Add (new Puzzle ("t0N42L3N34S0N47T1N83", 0));

		hardcore_puzzle.Add (new Puzzle ("t0N37L2F35B0N72S0N62T1N87", 7)); 
		hardcore_puzzle.Add (new Puzzle ("t0N33L1F32O0N83S1N77T1N55", 14)); // good puzzle.
		hardcore_puzzle.Add (new Puzzle ("t0N82L0F84O0N75S0F57T3N24", 15)); // great puzzle.
		hardcore_puzzle.Add (new Puzzle ("t0N23L0N77O0N47S0F42T2N45", 10)); // fantastic puzzle.

		hardcore_puzzle.Add (new Puzzle ("t0N38S1N76L0F46O0N73T3N43",13));	// an awesome puzzle.
		hardcore_puzzle.Add (new Puzzle ("t0N74L0F84O0N66S0F77T3N27", 12)); // fantastic. very hard.
		hardcore_puzzle.Add (new Puzzle ("t0N68L2N54O0N87S1F27T0N73", 11)); // this one is fantastic.
		hardcore_puzzle.Add (new Puzzle ("t0N32S1F25L3F73O0N65T2N77", 11)); // fantastic puzzle.


		/* spare levels:
		 * 
		 * "t0N58L1N38T3N24O0N42S0N84", this is too easy (but suitable for a tutorial level in physical game).
		 * "t0N84O0N47S1F77T2N44",      this is too easy (but suitable for a tutorial level in physical game).
		 * "t0N56S1N27T0N33",           this is an easier version of another puzzle.
		 * "t0N27L0F33S0F46O0N74",      easy. good for physical game?
		 * "t0N23L3N37S0N63T1N66",      easy. good for physical game?
		 * "t0N64L2N85S0N52T3N45",      very easy puzzle. good for physical game?
		 * "t0N57L0N24O0N83S0N42",      good for physical game?
		 * 
		 * these puzzles are good, but are just too big:
		 * 
		 * "t0N27L3F82O0N22S0N86T3N46", 
		 * "t0N75O0N68S3N25T3N54L2N95", 
		 * "t0N27L1N24O0N28S1N78T1N57", good.
		 * "t0N39L0F87O0N73S0N68T1N26", good 
		 * "t0N24L1N47O0N93S1N41T0N88", good puzzle 
		 * "t0N84L0N14O0N63S0N32T0N67", good 
		 * "t0N78L2N72O0N42T3N37", very good 
		 * "t0N68L0F63O0N23S0N44T2N83", good one
		 * "t0N37L0N33O0N86S0N74", this one is very similar to the L-only puzzle. (but it has no B-blocks in it)
		 * "t0N22L1N85O0N68S1N62T0N33", very good 
		 * "t0N43L0F96O0N91S1N26T3N73", very good puzzle 
		 * "t0N63L0N28O0N44S1N84T2N23", great puzzle 
		 * "t0N63L2F18O0N88S1N65T0N77", super hard and good.
		 * "t0N42L2F55O0N22S1N63T0N77", super awesome puzzle
		 * "t0N82L1N49O0N85S0N78T0N25", super awesome 
		 * "t0N26L2N87O0N75S0F21T1N43", this one is awesome!!! but doesn't fit on a 7x7.
		 * "t0N36L2N44O0N97S0F82T2N32", this one is fantastic.
		 * "t0N44L3N42O0N28S1F77T3N15", AWESOME!! super hard (but on a 9x9). The reason why this one is awesome, is that you have to cycle the tooth and the T-piece around one another alot.
		 */




		current_level = 1;
		hardcore_mode = false;

		/******************************** BEGIN HACK MODE **********************************/
		if (PlayerPrefs.HasKey("NormalProgress")) {
			normal_progress = PlayerPrefs.GetString ("NormalProgress");
		} else {
			normal_progress = "NU" + new string('L', 35);
			PlayerPrefs.SetString ("NormalProgress", normal_progress);
		}
		if (PlayerPrefs.HasKey("HardcoreProgress")) {
			hardcore_progress = PlayerPrefs.GetString ("HardcoreProgress");
		} else {
			hardcore_progress = "HU" + new string('L', 35);
			PlayerPrefs.SetString ("HardcoreProgress", hardcore_progress);
		} 
		/* 
		normal_progress = "N" + new string('A', 36);
		hardcore_progress = "H" + new string('A', 36);
		*/
		/******************************** END HACK MODE **********************************/


		if (PlayerPrefs.HasKey("BackgroundGrid")) {
			background_grid = PlayerPrefs.GetInt ("BackgroundGrid");
		} else {
			background_grid = 0;
			PlayerPrefs.SetInt ("BackgroundGrid", background_grid);
		}

		if (PlayerPrefs.HasKey("SoundVolume")) {
			sound_volume = PlayerPrefs.GetInt ("SoundVolume");
		} else {
			sound_volume = 70;
			PlayerPrefs.SetInt ("SoundVolume", sound_volume);
		}

	}

	public static void unlock_level(int index) {
		if (index >= 1 && index <= 36) {
			if (hardcore_mode) {
				for (int i = 1; i <= index; i++) {
					if (hardcore_progress.Substring (i, 1) == "L") {
						hardcore_progress = hardcore_progress.Substring (0, i) + "U" + hardcore_progress.Substring (i + 1, 36 - i);
					}
				}
				PlayerPrefs.SetString ("HardcoreProgress", hardcore_progress);
			} else {
				for (int i = 1; i <= index; i++) {
					if (normal_progress.Substring (i, 1) == "L") {
						normal_progress = normal_progress.Substring (0, i) + "U" + normal_progress.Substring (i + 1, 36 - i);
					}
				}
				PlayerPrefs.SetString ("NormalProgress", normal_progress);
			}
		}
	}

	public static bool solveLevel(int index, int num_moves) 
	{
		if (hardcore_mode) {
			bool star_finish = false;
			if (num_moves <= hardcore_puzzle [index].min_moves) { 
				star_finish = true;
				hardcore_progress = hardcore_progress.Substring (0, index)
					+ "A" + hardcore_progress.Substring (index + 1, 36 - index);
			} else if (hardcore_progress.Substring (index,1)!="A") {
				hardcore_progress = hardcore_progress.Substring (0, index)
					+ "B" + hardcore_progress.Substring (index + 1, 36 - index);
			} 
			unlock_level (index + 1);
			PlayerPrefs.SetString ("HardcoreProgress", hardcore_progress);
			return star_finish;
		} else {
			bool star_finish = false;
			if (num_moves <= normal_puzzle [index].min_moves) { 
				star_finish = true;
				normal_progress = normal_progress.Substring (0, index)
					+ "A" + normal_progress.Substring (index + 1, 36 - index);
			} else if (normal_progress.Substring (index,1)!="A") {
				normal_progress = normal_progress.Substring (0, index)
					+ "B" + normal_progress.Substring (index + 1, 36 - index);
			} 
			unlock_level (index + 1);
			PlayerPrefs.SetString ("NormalProgress", normal_progress);
			return star_finish;
		}
	}

	public static void restartNormalProgress() 
	{
		string temp = new string('L', 35);
		normal_progress = "NU"+temp;
		PlayerPrefs.SetString ("NormalProgress", normal_progress);
		current_level = 1;
	}

	public static void restartHardcoreProgress() 
	{
		string temp = new string('L', 35);
		hardcore_progress = "HU"+temp;
		PlayerPrefs.SetString ("HardcoreProgress", hardcore_progress);
		current_level = 1;
	}

	public static void setBackgroundGrid(bool bg_grid) 
	{
		if (bg_grid) {
			background_grid = 1;
		} else {
			background_grid = 0;
		}
		PlayerPrefs.SetInt ("BackgroundGrid",background_grid);
	}

	public static void setSoundVolume(int vol) 
	{
		sound_volume = vol;
		PlayerPrefs.SetInt ("SoundVolume",sound_volume);
	}


}
