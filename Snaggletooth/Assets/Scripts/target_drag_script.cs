﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class target_drag_script : MonoBehaviour {

	private float speed = 4.0f;

	void Update () {
		float translation = Time.deltaTime * speed;
		transform.Translate(translation,0.0f,0.0f);
	}
}
